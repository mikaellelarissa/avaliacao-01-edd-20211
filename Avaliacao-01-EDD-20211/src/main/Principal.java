package main;

import application.Cliente;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	static List<Cliente> cliente = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String nome, telefone, email;
		Cliente c = new Cliente();

		char escolha = 's';
		while(escolha == 's') {
			System.out.println("ARMAZENAR DADOS DO CLIENTE");
			System.out.println("Informe o nome do cliente: ");
			nome = sc.nextLine();
			System.out.println("Informe o email do cliente: ");
			email = sc.nextLine();
			System.out.println("Informe o telefone do cliente: ");
			telefone = sc.nextLine();

			c = new Cliente();
			c.add(cliente, c);
			System.out.println("Deseja armazenar os dados de um novo cliente? \n(s) sim      (n) n�o");
			escolha = sc.nextLine().charAt(0);
		}

		String r;
		System.out.println("Deseja consultar o cliente? \n(s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			String teste = " ";
			if (c.contains(cliente, c)) {
				teste = "existe.";
			} else {
				teste = "n�o existe.";
			}
			System.out.println("O cliente " + teste);
		}

		System.out.println("Deseja remover um cliente? \n(s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			c.remove(cliente, c);
			System.out.println("Livro removido!");
		}

		System.out.println("A quantidade de clientes � " + c.size(cliente));

		System.out.println("Deseja limpar a lista de clientes? \n(s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			c.clear(cliente);
			System.out.println("Lista limpa!");
			System.out.println("A quantidade de cliente agora � " + c.size(cliente));
		} else {
			System.out.println("A quantidade de clientes � " + c.size(cliente));
		}
	}

}

