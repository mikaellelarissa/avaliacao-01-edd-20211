package tui;

import java.util.List;

import application.Cliente;

public interface ClienteSet {

	public boolean contains(List<Cliente> cliente, Cliente cliente1);

	public boolean add(List<Cliente> cliente, Cliente cliente1);

	public boolean remove(List<Cliente> cliente, Cliente cliente1);

	public int size(List<Cliente> cliente);

	public void clear(List<Cliente> cliente);
	
}
